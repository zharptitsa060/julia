function NOD(num1, num2) {
    let less = num1 < num2 ? num2 : num1;

    for (let i = less; i !== 0; i--) {
        if (num1 % i === 0 && num2 % i === 0) {
            return i;
        }

    }

}

document.getElementById('result').innerText = NOD(22, 110);
